import { Elysia } from "elysia";
import { CharacterEvent, apply } from "./character-events";
import { CommandHandler } from "./character-commands";
import { CHARACTER_CLASS } from "./character";
import EventEmitter from "events";
import { EventStore } from "./event-store";
import { NumClassesProjector } from "./num-classes-projector";
import { actor } from "./character-actor";

// Bootstrapping
const myEmitter = new EventEmitter();
const eventStore = new EventStore<CharacterEvent>([], myEmitter);
const proj = new NumClassesProjector(myEmitter);

const commandHandler = CommandHandler(() => eventStore, actor);

// The app
const app = new Elysia()
    .post("/command/:aggregateId/name-character", async ({ params, body }) => {
        const { aggregateId } = params;
        // Request level validation goes in the controller/api layer
        if (!aggregateId) {
            throw new Error("No aggregate id!");
        }
        const { name } = body as { name: string };
        commandHandler(aggregateId, { type: "name-character", name });
        return new Response("OK");
    })
    .post("/command/:aggregateId/pick-character-class", ({ params, body }) => {
        const { aggregateId } = params;
        if (!aggregateId) {
            throw new Error("No aggregate id!");
        }
        const { characterClass } = body as { characterClass: string };
        if (
            characterClass !== CHARACTER_CLASS.WIZARD &&
            characterClass !== CHARACTER_CLASS.WARRIOR
        ) {
            throw new Error("pick a good class pls");
        }
        commandHandler(aggregateId, { type: "choose-class", characterClass });
        return new Response("OK");
    })
    .get("/stats/characters-by-class", () => {
        return new Response(JSON.stringify(proj.getNums()));
    })
    .get("/event-store", () => {
        return new Response(eventStore.getAllEvents());
    });

app.listen(3000);

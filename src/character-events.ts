import { CHARACTER_CLASS_HITPOINTS, Character, CharacterClass } from "./character";
import { assertNever } from "./utils";

export type CharacterEvent =
    | {
          type: "character-named";
          name: string;
      }
    | {
          type: "character-class-chosen";
          characterClass: CharacterClass;
      }
    | {
          type: "damage-taken";
          amount: number;
      }
    | {
          type: "character-healed";
          amount: number;
      };

export function apply(character: Character, event: CharacterEvent): Character {
    switch (event.type) {
        case "character-named":
            return { ...character, name: event.name };
        case "character-class-chosen":
            return {
                ...character,
                characterClass: event.characterClass,
                hitPoints: CHARACTER_CLASS_HITPOINTS[event.characterClass],
            };
        case "damage-taken":
            if (character.hitPoints === null) {
                throw new Error(
                    "Cannot fight until you have chosen a character",
                );
            }
            return {
                ...character,
                hitPoints: Math.max(0, character.hitPoints - event.amount),
            };
        case "character-healed":
            if (character.hitPoints === null) {
                throw new Error(
                    "Channot heal until you have chosen a character",
                );
            }
            const maxHitPoints = character.characterClass === "wizard" ? 4 : 8;
            return {
                ...character,
                hitPoints: Math.min(
                    maxHitPoints,
                    character.hitPoints + event.amount,
                ),
            };
        default:
            return assertNever(event);
    }
}


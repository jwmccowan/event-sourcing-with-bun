import EventEmitter from "events";


export class EventStore<T> {
    public constructor(
        private events: [aggregateId: string, event: T][] = [],
        // putting this here because I'm lazy but it should be event driven
        private eventEmitter: EventEmitter,
    ) {}

    public publishEvent(event: [aggregateId: string, event: T]) {
        this.events.push(event);
        this.eventEmitter.emit("event-published", event);
    }

    public getEventsFor(aggregateId: string): T[] {
        return this.events
            .filter(([id]) => id === aggregateId)
            .map(([_, event]) => event);
    }

    public getAllEvents(): string {
        return JSON.stringify(this.events);
    }
}

import EventEmitter from "events";
import { CharacterClass } from "./character";
import { CharacterEvent } from "./character-events";

export class NumClassesProjector {
    public constructor(
        private myEmitter: EventEmitter,
        private nums: Record<CharacterClass, number> = {
            wizard: 0,
            warrior: 0,
        },
    ) {
        this.myEmitter.on(
            "event-published",
            (ev: [aggregateId: string, event: CharacterEvent]) => {
                if (ev[1].type === "character-class-chosen") {
                    this.nums[ev[1].characterClass]++;
                    console.log("eggs", this.nums);
                }
            },
        );
    }

    public getNums() {
        return { ...this.nums };
    }
}

import { ObjectValues } from "./utils"

export const CHARACTER_CLASS = {
    WARRIOR: "warrior",
    WIZARD: "wizard",
} as const;

export type CharacterClass = ObjectValues<typeof CHARACTER_CLASS>;

export const CHARACTER_CLASS_HITPOINTS: Record<CharacterClass, number> = {
    wizard: 4,
    warrior: 8,
};

export type Character = {
    aggregateId: string;
    name: string | null;
    characterClass: string | null;
    hitPoints: number | null;
};

export function getEmptyCharacter(aggregateId: string): Character {
    return { aggregateId, characterClass: null, name: null, hitPoints: null };
}

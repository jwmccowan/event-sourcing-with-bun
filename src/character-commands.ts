import { Character, CharacterClass } from "./character";
import { Actor } from "./character-actor";
import { CharacterEvent } from "./character-events";
import { EventStore } from "./event-store";
import { assertNever } from "./utils";

export type CharacterCommand =
    | {
          type: "name-character";
          name: string;
      }
    | {
          type: "choose-class";
          characterClass: CharacterClass;
      }
    | {
          type: "attack";
          amount: number | null;
      }
    | {
          type: "vampireBlast";
          amount: number | null;
          attacker: Character;
      };

export function act(
    command: CharacterCommand,
    character: Character,
): CharacterEvent | CharacterEvent[] {
    switch (command.type) {
        case "name-character":
            return { type: "character-named", name: command.name };
        case "choose-class":
            if (character.characterClass !== null) {
                throw new Error("Character class already chosen");
            }
            return {
                type: "character-class-chosen",
                characterClass: command.characterClass,
            };
        case "attack":
            if (character.hitPoints === null) {
                throw new Error(
                    "Character has not been created - try picking a class",
                );
            }
            const damageDealt = command.amount ?? 3;
            return { type: "damage-taken", amount: damageDealt };
        case "vampireBlast":
            if (character.hitPoints === null) {
                throw new Error(
                    "Character has not been created - try picking a class",
                );
            }
            if (command.attacker.hitPoints === null) {
                throw new Error(
                    "Attacker has not been created - try picking a class",
                );
            }
            const spellDamageDealt = command.amount ?? 1;
            if (command.attacker.hitPoints - spellDamageDealt <= 0) {
                throw new Error(
                    "Attacker does not have enough hitpoints to cast this spell",
                );
            }
            return [
                { type: "damage-taken", amount: spellDamageDealt },
                {
                    type: "character-healed",
                    amount: Math.ceil(spellDamageDealt / 2),
                },
            ];
        default:
            return assertNever(command);
    }
}

function isCharacterEvent(
    events: CharacterEvent | CharacterEvent[],
): events is CharacterEvent {
    return !Array.isArray(events);
}

export function CommandHandler(getEventStore: () => EventStore<CharacterEvent>, actor: Actor) {
    return (aggregateId: string, command: CharacterCommand) => {
        const eventStore = getEventStore();

        const events = eventStore.getEventsFor(aggregateId);

        const character = events.reduce(
            actor.apply,
            actor.getEmptyCharacter(aggregateId),
        );

        const newEvents = actor.act(command, character);

        const newEventsAsArray = isCharacterEvent(newEvents)
            ? [newEvents]
            : newEvents;

        newEventsAsArray.forEach((event) =>
            eventStore.publishEvent([character.aggregateId, event]),
        );
    };
}

export function assertNever(_a: unknown): never {
    throw new Error("Exhaustive check failed");
}

export type ObjectValues<T> = T[keyof T];

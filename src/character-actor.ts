import { Character, getEmptyCharacter } from "./character";
import { CharacterCommand, act } from "./character-commands";
import { CharacterEvent, apply } from "./character-events";

export type Actor = {
    act: (
        command: CharacterCommand,
        character: Character,
    ) => CharacterEvent | CharacterEvent[];
    apply: (character: Character, event: CharacterEvent) => Character;
    getEmptyCharacter: (aggregateId: string) => Character;
};

export const actor: Actor = {
    act,
    apply,
    getEmptyCharacter,
};
